from dash.dependencies import Input, Output

import controller
from app import app, server  # noqa
from database.transforms import df
from tabs import page, tab1, tab2
from tabs.data_processing import rename_cols
from utils.str_functions import float_to_reais

app.layout = page.layout


@app.callback(Output("tabs-content", "children"), [Input("tabs", "value")])
def render_content(tab):
    """Aciona tabs de acordo com seleção."""
    if tab == "tab-1":
        return tab1.layout
    elif tab == "tab-2":
        return tab2.layout


@app.callback(
    [
        Output("table-sorting-filtering", "data"),
        Output("toast-value-total", "children"),
        Output("toast-value-honor", "children"),
        Output("toast-value-subc", "children"),
    ],
    [
        Input("table-sorting-filtering", "page_current"),
        Input("table-sorting-filtering", "page_size"),
        Input("table-sorting-filtering", "sort_by"),
        Input("table-sorting-filtering", "filter_query"),
        Input("agencia_filter", "value"),
        Input("campanha_filter", "value"),
        Input("subcontratada_filter", "value"),
        Input("contrato_filter", "value"),
    ],
)
def update_table(
    page_current,
    page_size,
    sort_by,
    filter,
    agencias,
    campanhas,
    subcontratadas,
    contratos,
):
    """Atualiza a tabela principal."""
    dff = df.copy()

    # col_name: parameter
    filters = {
        "agencia": agencias,
        "descricao_campanha": campanhas,
        "subcontratada": subcontratadas,
        "contrato": contratos,
    }

    if len(sort_by):
        dff = controller.sort_by(dff, sort_by)

    dff = controller.filter_df(dff, filters)

    vl_total = float_to_reais(dff["valor_total"].sum())
    vl_honor = float_to_reais(dff["honor"].sum())
    vl_subc = float_to_reais(dff["vl_subc"].sum())

    dff["valor_total"] = dff["valor_total"].apply(float_to_reais)
    dff["honor"] = dff["honor"].apply(float_to_reais)
    dff["vl_subc"] = dff["vl_subc"].apply(float_to_reais)

    dff = rename_cols(dff)

    dff = controller.filter_by_page_size(dff, page_current, page_size)

    return dff, vl_total, vl_honor, vl_subc


if __name__ == "__main__":
    app.run_server(debug=True)
