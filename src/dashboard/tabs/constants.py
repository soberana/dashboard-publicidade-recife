#!/usr/bin/env ipython

TITLE = "GASTOS COM PUBLICIDADE DA PREFEITURA DO RECIFE"

TXT_INTRO = (
    "Esse é o valor que a Prefeitura do Recife gastou com Publicidade"
    " durante os anos de 2021 e 2022. Nosso mandato, junto com a sociedade civil, "
    "cobrou e  garantiu que a Lei Federal nº 12.232/2010 fosse cumprida no "
    "Recife e essas despesas fossem especificadas no Portal da Transparência."
    "Aqui, a gente apresenta essas informações detalhadas para consulta, "
    "de uma forma mais simples e interativa. Estão disponíveis apenas os "
    "dados do período entre Janeiro de 2021 e Agosto de 2022. Para além "
    "disso, não temos como saber, pois os dados não são divulgados."
)

HASHTAG = "#PRAONDEVAINOSSODINHEIRO"

FOOTER = "Desenvolvido por @"
