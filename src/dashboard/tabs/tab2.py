import plotly.graph_objs as go
from dash import dcc, html
from dash.dependencies import Input, Output

from app import app
from database.transforms import df

layout = html.Div(id="table-paging-with-graph-container", className="five columns")


@app.callback(
    Output("table-paging-with-graph-container", "children"),
    [Input("rating-95", "value"), Input("price-slider", "value")],
)
def update_graph(ratingcheck, prices):
    dff = (
        df.groupby("subcontratada")["vl_subc"]
        .sum()
        .reset_index()
        .sort_values("vl_subc", ascending=False)
    )

    low = prices[0]
    high = prices[1]

    # dff = dff.loc[(dff["price"] >= low) & (dff["price"] <= high)]

    if ratingcheck == ["Y"]:
        dff = dff.nlargest(n=10, columns=["vl_subc"])
        # dff.loc[dff["rating"] >= 95]
    else:
        dff

    trace1 = go.Bar(
        x=dff["subcontratada"],
        y=dff["vl_subc"],
        # mode="markers",
        opacity=0.7,
        # marker={"size": 8, "line": {"width": 0.5, "color": "white"}},
        name="Price v Rating",
    )
    return html.Div(
        [
            dcc.Graph(
                id="rating-price",
                figure={
                    "data": [
                        trace1
                        # dict(
                        #     x=df['price'],
                        #     y=df['rating'],
                        #     #text=df[df['continent'] == i]['country'],
                        #     mode='markers',
                        #     opacity=0.7,
                        #     marker={
                        #         'size': 8,
                        #         'line': {'width': 0.5, 'color': 'white'}
                        #     },
                        #     name='Price v Rating'
                        # )
                    ],
                    "layout": dict(
                        # xaxis={"type": "log", "title": "Rating"},
                        yaxis={"title": "Price"},
                        margin={"l": 40, "b": 40, "t": 10, "r": 10},
                        legend={"x": 0, "y": 1},
                        hovermode="closest",
                    ),
                },
            )
        ]
    )
