import dash_bootstrap_components as dbc
from dash import dcc, html

from database.transforms import df
from tabs import constants, tab1

min_p = df["valor_total"].min()
max_p = df["valor_total"].max()

orgs = ["TODOS"] + list(df["org"].unique())
agencias = ["TODOS"] + list(df["agencia"].unique())
campanhas = ["TODOS"] + list(df["descricao_campanha"].unique())
subcontratadas = ["TODOS"] + list(df["subcontratada"].unique())
contratos = ["TODOS"] + list(df["contrato"].unique())


layout = html.Div(
    [
        dbc.Container(
            [
                dbc.Row([html.H2("Filtros"), html.Hr(), ],
                        className="row-filtros"),
                dbc.Row(
                    [
                        dbc.Col(
                            [
                                html.H5("Agências"),
                                dcc.Dropdown(
                                    agencias, "TODOS", id="agencia_filter", multi=True,
                                ),
                            ]
                        ),
                        dbc.Col(
                            [
                                html.H5("Campanhas"),
                                dcc.Dropdown(
                                    campanhas,
                                    "TODOS",
                                    id="campanha_filter",
                                    multi=True,
                                ),
                            ]
                        ),
                        dbc.Col(
                            [
                                html.H5("Subcontratadas"),
                                dcc.Dropdown(
                                    subcontratadas,
                                    "TODOS",
                                    id="subcontratada_filter",
                                    multi=True,
                                ),
                            ]
                        ),
                        dbc.Col(
                            [
                                html.H5("Contratos"),
                                dcc.Dropdown(
                                    contratos,
                                    "TODOS",
                                    id="contrato_filter",
                                    multi=True,
                                ),
                            ]
                        ),
                    ]
                ),
                html.Div(
                    [
                        dbc.Row(
                            [
                                dbc.Col(
                                    dbc.Toast(
                                        id="toast-value-total", header="Valor Total",
                                    )
                                ),
                                dbc.Col(
                                    dbc.Toast(
                                        id="toast-value-honor",
                                        header="Valor Honorários",
                                    )
                                ),
                                dbc.Col(
                                    dbc.Toast(
                                        id="toast-value-subc",
                                        header="Valor Subcontratadas",
                                    )
                                ),
                            ],
                            className="toast-row",
                        ),
                    ],
                    id="div-table-sum",
                ),
                dbc.Row(
                    [
                        html.Div(
                            [
                                dcc.Tabs(
                                    id="tabs",
                                    value="tab-1",
                                    children=[
                                        dcc.Tab(label="Tabela", value="tab-1"),
                                        dcc.Tab(label="Gráficos",
                                                value="tab-2"),
                                    ],
                                    className="my-4",
                                ),
                                html.Div(id="tabs-content",
                                         children=tab1.layout),
                            ]
                        ),
                    ]
                ),
            ],
            className="abas-container content-container",
        ),
        html.Div(
            [
                dbc.Row(
                    [
                        dbc.Button(
                            html.I(className="bi bi-arrow-up"),
                            className="btn-back-top",
                            href="#navbar",
                        ),
                    ],
                    className="row-btn-back-top",
                ),
            ]
        ),
    ],
    className="div-tabs",
    id="dados",
)
