from dash import dash_table, html
from dash.dependencies import Input, Output

from app import app
from database.transforms import df
from tabs.data_processing import rename_cols

PAGE_SIZE = 50
columns = rename_cols(df).columns

layout = html.Div(
    [
        dash_table.DataTable(
            id="table-sorting-filtering",
            columns=[{"name": i, "id": i, "deletable": True} for i in columns],
            style_table={"height": "750px", "overflowX": "scroll"},
            style_data_conditional=[
                {"if": {"row_index": "odd"},
                    "backgroundColor": "rgb(248, 248, 248)"}
            ],
            style_cell={
                "height": "90",
                # all three widths are needed
                "minWidth": "150px",
                "width": "150px",
                "maxWidth": "200px",
                "textAlign": "center",
                "whiteSpace": "normal",
            },
            page_current=0,
            page_size=PAGE_SIZE,
            page_action="custom",
            filter_action="none",
            sort_action="custom",
            sort_mode="multi",
            sort_by=[],
            export_format="csv",
            hidden_columns=["empenho", "ano_empenho",
                            "subempenho", "subelemento"],
        ),
        html.Button("Download da tabela", className="export-table-btn",
                    id="export_table", **{"data-dummy": ""}),
    ]
)


app.clientside_callback(
    """
    function(n_clicks) {
        if (n_clicks > 0)
            document.querySelector("#table-sorting-filtering button.export").click()
        return ""
    }
    """,
    Output("export_table", "data-dummy"),
    [Input("export_table", "n_clicks")],
)
