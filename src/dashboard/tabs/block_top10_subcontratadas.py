#!/usr/bin/env ipython
import dash
import dash_bootstrap_components as dbc
import pandas as pd
import plotly.graph_objs as go
from dash import dash_table, dcc, html
from dash.dependencies import Input, Output

from app import app
from database.transforms import df
from tabs import constants

dff = (
    df.groupby("subcontratada")["vl_subc"]
    .sum()
    .reset_index()
    .sort_values("vl_subc", ascending=False)
)

dff = dff.nlargest(n=10, columns=["vl_subc"])
trace1 = go.Bar(
    x=dff["subcontratada"],
    y=dff["vl_subc"],
    opacity=0.7,
    name="Top 10 - Subcontratadas",
)

layout = html.Div(
    [
        dbc.Container(
            [
                dbc.Row(
                    html.P(constants.TXT_INTRO, className="txt-intro content-container")
                ),
            ]
        ),
        dbc.Container(
            [
                dbc.Row(
                    [
                        html.H2("Top 10 - Subcontratadas", className="py-2 mb-2"),
                        dcc.Graph(
                            figure={
                                "data": [trace1],
                                "layout": dict(
                                    yaxis={"title": "Valor"},
                                    margin={"l": 40, "b": 40, "t": 10, "r": 10},
                                    legend={"x": 0, "y": 1},
                                    hovermode="closest",
                                    config={"locale": "pt-BR"},
                                ),
                            },
                        ),
                    ],
                    className="graph-subcontratadas content-container",
                )
            ]
        ),
        html.Div(
            [
                dbc.Row(
                    [
                        dbc.Button(
                            html.I(className="bi bi-arrow-up"),
                            className="btn-back-top",
                            href="#navbar",
                        ),
                    ],
                    className="row-btn-back-top",
                ),
            ]
        ),
    ],
    className="div-subcontratadas",
    id="subcontratadas",
)
