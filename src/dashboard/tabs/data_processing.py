from database.replace_values import REPLACE_COLS


def rename_cols(df):
    df = df.rename(columns=REPLACE_COLS)
    return df
