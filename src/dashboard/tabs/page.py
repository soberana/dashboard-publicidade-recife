import dash_bootstrap_components as dbc
from dash import Input, Output, State, html

from database.transforms import df
from tabs import block_top10_subcontratadas, constants, sidepanel
from utils.str_functions import float_to_reais
from app import app

publi_total_value = float_to_reais(df["valor_total"].sum())
icone_brand = "../assets/img/ivan-moraes-psol.png"

navbar = dbc.Navbar(
    dbc.Container(
        [
            html.A(
                # Use row and col to control vertical alignment of logo / brand
                dbc.Row(
                    [
                        dbc.Col(html.Img(src=icone_brand, height="30px")),
                        dbc.Col(dbc.NavbarBrand("GPPR", className="ms-2")),

                    ],
                    align="center",
                    className="g-0",
                ),
                href="#",
                style={"textDecoration": "none"},
            ),
            dbc.NavbarToggler(id="navbar-toggler", n_clicks=0),
            dbc.Collapse([
                dbc.Row([
                    dbc.Nav(
                        [
                            dbc.NavItem(dbc.NavLink(
                                "Sobre", href="#sobre", external_link=True)),
                            dbc.NavItem(dbc.NavLink(
                                "Top 10 - Subcontratadas", href="#subcontratadas", external_link=True)),
                            dbc.NavItem(dbc.NavLink(
                                "Dados", href="#dados", external_link=True)),
                        ])
                ],
                    className="g-0 ms-auto")

            ],
                id="navbar-collapse",
                is_open=False,
                navbar=True
            ),

        ]
    )
)

# add callback for toggling the collapse on small screens


@ app.callback(
    Output("navbar-collapse", "is_open"),
    [Input("navbar-toggler", "n_clicks")],
    [State("navbar-collapse", "is_open")],
)
def toggle_navbar_collapse(n, is_open):
    if n:
        return not is_open
    return is_open


title = html.H1(constants.TITLE, className="title-page p-4 mb-2 text-center")

intro = html.Div(
    [
        dbc.Container(
            [
                dbc.Row([
                    html.Img(src="../assets/img/testeira.png",
                             className="testeira"),
                ]),
                dbc.Row(
                    html.H2(
                        children=publi_total_value,
                        className="div-valor-total text-center display-3 py-3 rounded",
                    )
                ),
                dbc.Row(html.P(constants.TXT_INTRO, className="txt-intro")),
                dbc.Row(html.H2(constants.HASHTAG, className="text-center")),
                dbc.Row(html.Img(src="../assets/img/ivan-moraes-psol.png",
                        className="candidato-icon"), className="row-candidato-icon"),
            ],
            className="intro-container content-container",
        )
    ],
    className="div-intro",
    id="sobre",
)


footer = html.Footer(html.P(constants.FOOTER),
                     className="footer p-5 mb-2 text-center")


layout = html.Div(
    [
        navbar,
        intro,
        block_top10_subcontratadas.layout,
        sidepanel.layout,
        footer,
    ],
    className="page-container",
)
