#!/usr/bin/env python3
def float_to_reais(f):
    f_str = str(f)
    integer, decimal = f_str.split(".")

    if len(decimal) < 2:
        decimal = decimal + "0"
    else:
        decimal = decimal[:2]

    integer_with_dots = ".".join(reversed(break_in_3(integer, [])))

    return f"R$ {integer_with_dots},{decimal}"


def break_in_3(s, l):
    if len(s) <= 3:
        l.append(s)
        return l
    else:
        l.append(s[-3:])
        return break_in_3(s[:-3], l)
