#!/usr/bin/env python3
from functools import reduce


def all_in_all_filters(list_of_filters):
    """Verifica se todos os filtros tem o valor TODOS."""
    list_of_alls = ["TODOS" in f for f in list_of_filters]
    return all(list_of_alls)


def true_or_filter(serie, filter_list):
    """Gerar filtro caso o valor TODOS não for definido."""
    _filter = True if "TODOS" in filter_list else serie.isin(filter_list)
    return _filter


def filter_df(df, filters):
    """Filtra o DataFrame caso necessário."""
    if not all_in_all_filters(filters.values()):
        df_filter_tuple = (
            true_or_filter(df[col], filter_list) for col, filter_list in filters.items()
        )
        df_filter = reduce(lambda x, y: x & y, df_filter_tuple)

        df = df[df_filter]

    return df


def filter_by_range(df, col, low, high):
    """Filtra uma coluna de acordo com o range passado."""
    df = df.loc[(df[col] >= low) & (df[col] <= high)]
    return df


def sort_by(df, sort_by):
    """Ordena o DataFrame baseado no parâmetro sort_by (dash_table)."""
    df = df.sort_values(
        [col["column_id"] for col in sort_by],
        ascending=[col["direction"] == "asc" for col in sort_by],
        inplace=False,
    )
    return df


def filter_by_page_size(df, page_current, page_size):
    """Filtro a partir dos valores passados na paginação."""
    df = df.iloc[(page_current * page_size) : ((page_current + 1) * page_size)].to_dict(
        "records"
    )
    return df


def filter_by_top_n(df, col, n):
    """Filtra os TOP N valores de uma coluna."""
    df = df.nlargest(n=n, columns=[col]).sort_values(col)
