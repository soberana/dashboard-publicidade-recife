import dash
import dash_bootstrap_components as dbc

app = dash.Dash(
    __name__, external_stylesheets=[dbc.themes.BOOTSTRAP, dbc.icons.BOOTSTRAP]
)

app.title = "Gastos da Prefeitura do Recife com Publicidade"

server = app.server
app.config.suppress_callback_exceptions = True
