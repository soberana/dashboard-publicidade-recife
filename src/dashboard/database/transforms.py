#!/usr/bin/env ipython
import pandas as pd

from database.replace_values import REPLACE_VALUES_DICT


def transform_df():
    """Gera DataFrame para uso na aplicação."""
    df = pd.read_csv(
        "https://s3.us-east-1.wasabisys.com/scrap-publicidade/publicidade_recife/df_app.csv"
    )
    df = map_values(df)
    return df


def map_values(df):
    "Função de desambiguação"
    return df.replace(REPLACE_VALUES_DICT)


df = transform_df()
