#!/usr/bin/env python3
REPLACE_VALUES_DICT = {"EDUCACIONAL": "EDUCATIVO"}

REPLACE_COLS = {
    "org": "Secretaria",
    "agencia": "Agência",
    "descricao_campanha": "Campanha",
    "subcontratada": "Subcontratada",
    "contrato": "Contrato",
    "valor_total": "Valor Total",
    "honor": "Honorários",
    "vl_subc": "Valor Subcontratada",
}
